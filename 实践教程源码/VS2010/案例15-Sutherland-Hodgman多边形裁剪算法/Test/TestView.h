
// TestView.h : CTestView 类的接口
//

#pragma once

#include "P2.h"//包含点类
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void DoubleBuffer(CDC *pDC);//双缓冲
	void ReadPoint();//读入点表
	void DrawWindowRect(CDC *);//绘制裁剪窗口
	void DrawObject(CDC *,BOOL);//绘制多边形
	void ClipBoundary(CP2 ,CP2);//窗口边界赋值函数
	void ClipPolygon(CP2 *,int ,UINT);//裁剪多边形
	BOOL Inside(CP2 ,UINT);//点在窗口边界内判断函数
	CP2  Intersect(CP2 p0,CP2 p1,UINT Boundary);//计算交点函数
	CP2  Convert(CPoint point);//坐标系转换
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP2 *In,*Out;//裁剪前后的顶点数组
	CP2 Rect[2];//窗口顶点数组
	int OutCount;//裁剪后的顶点数组中顶点个数
    int RtCount;//窗口顶点个数
	double	Wxl,Wxr,Wyb,Wyt;//剪裁窗口边界
	BOOL bDrawRect;//是否允许绘制窗口
	BOOL bClip;//是否允许裁剪
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnDrawpic();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
//	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
//	afx_msg void OnClip();
//	afx_msg void OnUpdateClip(CCmdUI *pCmdUI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
//	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnZoomin();
//	afx_msg void OnZoomout();
	afx_msg void OnClip();
	afx_msg void OnUpdateClip(CCmdUI *pCmdUI);
	virtual void OnInitialUpdate();
	afx_msg void OnDrawpic();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

