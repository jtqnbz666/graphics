
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	CRect rect;
	GetClientRect(&rect);	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);	
	IFSCode(pDC);
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序

void CTestView::IFSCode(CDC *pDC)
{
	P=CP2(100,100);
	Code[0][0]=0.333;Code[0][1]=0;     Code[0][2]=0;     Code[0][3]=0.333;Code[0][4]=0;    Code[0][5]=0;    Code[0][6]=0.25;
	Code[1][0]=0.167;Code[1][1]=-0.289;Code[1][2]=0.289; Code[1][3]=0.167;Code[1][4]=0.333;Code[1][5]=0;    Code[1][6]=0.25;
	Code[2][0]=0.167;Code[2][1]=0.289; Code[2][2]=-0.289;Code[2][3]=0.167;Code[2][4]=0.5;  Code[2][5]=0.289;Code[2][6]=0.25;
	Code[3][0]=0.333;Code[3][1]=0;     Code[3][2]=0;     Code[3][3]=0.333;Code[3][4]=0.667;Code[3][5]=0;    Code[3][6]=0.25;
	for(int i=0;i<100000;i++)//分形图的浓度
	{
		double R=double(rand())/RAND_MAX;//RAND_MAX随机数的最大值
		if(R<=Code[0][6]) 
		{
			a=Code[0][0];b=Code[0][1];c=Code[0][2];d=Code[0][3];e=Code[0][4];f=Code[0][5];
		}
		else if(R<=Code[0][6]+Code[1][6])
		{
			a=Code[1][0];b=Code[1][1];c=Code[1][2];d=Code[1][3];e=Code[1][4];f=Code[1][5];
		}
		else if(R<=Code[0][6]+Code[1][6]+Code[2][6])
		{
			a=Code[2][0];b=Code[2][1];c=Code[2][2];d=Code[2][3];e=Code[2][4];f=Code[2][5];
		}
		else 
		{
			a=Code[3][0];b=Code[3][1];c=Code[3][2];d=Code[3][3];e=Code[3][4];f=Code[3][5];
		}
		P1.x=a*P.x+b*P.y+e;//仿射变换
		P1.y=c*P.x+d*P.y+f;
		P=P1;
		double k=500;//调节系数
		pDC->SetPixelV((Round(4/3*k*P.x)-250),(Round(k*P.y-300)+200),RGB(P.x*500*R,R*100,P.y*500*R));
	}
}

void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
}
