// InputDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Test.h"
#include "InputDlg.h"
#include "afxdialogex.h"


// CInputDlg 对话框

IMPLEMENT_DYNAMIC(CInputDlg, CDialogEx)

CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInputDlg::IDD, pParent)
	, m_m(4)
{

}

CInputDlg::~CInputDlg()
{
}

void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_m);
	DDV_MinMaxInt(pDX, m_m, 0, 10);
}


BEGIN_MESSAGE_MAP(CInputDlg, CDialogEx)
	ON_BN_CLICKED(IDC_RADIO1, &CInputDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CInputDlg::OnBnClickedRadio2)
END_MESSAGE_MAP()


// CInputDlg 消息处理程序


void CInputDlg::OnBnClickedRadio1()
{
	// TODO: 在此添加控件通知处理程序代码
	m_s=1;
}


void CInputDlg::OnBnClickedRadio2()
{
	// TODO: 在此添加控件通知处理程序代码
	m_s=-1;
}
