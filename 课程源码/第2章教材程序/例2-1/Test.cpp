#include <iostream.h>
class CRectangle                                   //定义长方形类
{
public:
	CRectangle();                                  //声明默认构造函数
	CRectangle(int width,int height);              //声明带参构造函数
	~CRectangle();                                 //声明析构函数
	double circum();                                //声明计算周长成员函数
	double area();                                  //声明计算面积成员函数
private:
	int width;                                     //声明长方形的宽度成员变量
	int height;                                    //声明长方形的高度成员变量
};
CRectangle::CRectangle()                            //定义默认构造函数
{
	width=10;
	height=5;	
	cout<<"建立默认对象"<<endl;
}
CRectangle::CRectangle(int width,int height)         //定义带参构造函数
{
	this->width=width;
this->height=height;	
	cout<<"建立对象"<<endl;
}
CRectangle::~CRectangle()                           //定义析构函数
{
	cout<<"撤销对象"<<endl;
}
double CRectangle::circum()                           //定义计算周长成员函数
{
	return 2*( width+height);
}
double CRectangle::area()                             //定义计算面积成员函数
{
	return width*height; 
}
void main()                                           //主函数
{
	CRectangle Rect1,Rect2(30,20),*pRect=&Rect2;      //定义对象和指向对象的指针
	//使用对象输出周长和面积
	cout<<"Rect1的周长为："<<Rect1.circum()<<endl;    //使用对象输出Rect1的周长
	cout<<"Rect1的面积为："<<Rect1.area()<<endl;      //使用对象输出Rect1的面积
	cout<<"Rect2的周长为："<<Rect2.circum()<<endl;    //使用对象输出Rect2的周长
	cout<<"Rect2的面积为："<<Rect2.area()<<endl;      //使用对象输出Rect2的面积	
	//使用对象指针输出Rect2的周长和面积
	cout<<"Rect2的周长为："<<pRect->circum()<<endl;   //使用指向对象的指针输出Rect2的周长
	cout<<"Rect2的面积为："<<pRect->area()<<endl;     //使用指向对象的指针输出Rect2的面积
}
