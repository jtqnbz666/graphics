// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(ID_MDRAW, OnMdraw)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here

}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);//显示DC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	CDC memDC;//声明一个内存DC
	memDC.CreateCompatibleDC(pDC);//创建一个与显示DC兼容的内存DC
	CBitmap NewBitmap,*pOldBitmap; 
	NewBitmap.LoadBitmap(IDB_ABOUT);//从资源中导入位图
	BITMAP bmpInfo; //声明bmpInfo结构体
	NewBitmap.GetBitmap(&bmpInfo);//获取位图信息	
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将位图选入内存DC
	memDC.SetMapMode(MM_ANISOTROPIC);//内存DC自定义坐标系
	memDC.SetWindowExt(bmpInfo.bmWidth,bmpInfo.bmHeight);
	memDC.SetViewportExt(bmpInfo.bmWidth,-bmpInfo.bmHeight);
	memDC.SetViewportOrg(bmpInfo.bmWidth/2,bmpInfo.bmHeight/2);
	int nX=rect.left+(rect.Width()-bmpInfo.bmWidth)/2;//计算位图在客户区的中心点
    int nY=rect.top+(rect.Height()-bmpInfo.bmHeight)/2;
	pDC->BitBlt(nX,nY,rect.Width(),rect.Height(),&memDC,-bmpInfo.bmWidth/2,-bmpInfo.bmHeight/2,SRCCOPY);//将内存DC中的位图拷贝到设备DC
//	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-bmpInfo.bmWidth/2,-bmpInfo.bmHeight/2,SRCCOPY);//将内存DC的位图拷贝到设备DC
//	pDC->StretchBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-bmpInfo.bmWidth/2,-bmpInfo.bmHeight/2,bmpInfo.bmWidth,bmpInfo.bmHeight,SRCCOPY);//拉伸位图
	memDC.SelectObject(pOldBitmap);//从内存DC中释放位图
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers

void CTestView::OnMdraw() 
{
	// TODO: Add your command handler code here
	
}
