// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"
#include "math.h"
#define Round(a) int(floor(a+0.5))
#define PI 3.1415926
#include "TestDoc.h"
#include "TestView.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(ID_MDRAW, OnMdraw)
	ON_UPDATE_COMMAND_UI(ID_MDRAW, OnUpdateMdraw)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	bPlay=FALSE;
	Theta=360;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);

	//不使用双缓冲画图
//	DrawObject(pDC);

	//使用双缓冲画图
	CDC memDC;                    //声明内存DC
	CBitmap NewBitmap,*pOldBitmap;
	memDC.CreateCompatibleDC(pDC);    //创建一个与显示DC兼容的内存DC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容内存位图 
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将兼容位图选入内存DC
	memDC.FillSolidRect(rect,pDC->GetBkColor());//按原来背景色填充客户区，否则是黑色
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	memDC.SetMapMode(MM_ANISOTROPIC);//内存DC自定义坐标系
	memDC.SetWindowExt(rect.Width(),rect.Height());
	memDC.SetViewportExt(rect.Width(),-rect.Height());
	memDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);

	DrawObject(&memDC);

	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-rect.Width()/2,-rect.Height()/2,SRCCOPY); //将内存DC中的位图拷贝到设备DC
	memDC.SelectObject(pOldBitmap);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers
void CTestView::DrawObject(CDC *pDC)
{
	CPen NewPen,*pOldPen;
	CBrush NewBrush,*pOldBrush;
	long R=300;//太极图半径
	CPoint ld,rt,as,ae;
	//ld(left down)左下角点,rt(right top)右上角点;
	//as(arc start)圆弧起点,ae(arc end)圆弧终点
	//绘制黑色半圆，见图A-1
	NewBrush.CreateSolidBrush(RGB(0,0,0));
	pOldBrush=pDC->SelectObject(&NewBrush);
	//0°角为x轴	
	ld=CPoint(-R,-R);rt=CPoint(R,R);
	as=CPoint(Round(R*cos((Theta-90)*PI/180)),Round(R*sin((Theta-90)*PI/180)));
    ae=CPoint(Round(R*cos((Theta+90)*PI/180)),Round(R*sin((Theta+90)*PI/180)));
	pDC->Pie(CRect(ld,rt),as,ae);
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();
	//绘制白色半圆，见图A-2
	NewBrush.CreateSolidBrush(RGB(255,255,255));
	pOldBrush=pDC->SelectObject(&NewBrush);
	ld=CPoint(-R,-R);rt=CPoint(R,R);
	as=CPoint(Round(R*cos((Theta+90)*PI/180)),Round(R*sin((Theta+90)*PI/180)));
    ae=CPoint(Round(R*cos((Theta-90)*PI/180)),Round(R*sin((Theta-90)*PI/180)));
	pDC->Pie(CRect(ld,rt),as,ae);
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();
	//绘制白色鱼头，见图A-3
	long r=R/2;//鱼头半径
	NewPen.CreatePen(PS_SOLID,1,RGB(255,255,255)); 
	pOldPen=pDC->SelectObject(&NewPen);
	ld=CPoint(Round(r*cos((Theta+90)*PI/180)-r),Round(r*sin((Theta+90)*PI/180)-r));
    rt=CPoint(Round(r*cos((Theta+90)*PI/180)+r),Round(r*sin((Theta+90)*PI/180)+r));
	pDC->Ellipse(CRect(ld,rt));
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
	//黑色鱼头，见图A-4
	NewBrush.CreateSolidBrush(RGB(0,0,0));
	pOldBrush=pDC->SelectObject(&NewBrush);
    ld=CPoint(Round(r*cos((Theta-90)*PI/180)-r),Round(r*sin((Theta-90)*PI/180)-r));
    rt=CPoint(Round(r*cos((Theta-90)*PI/180)+r),Round(r*sin((Theta-90)*PI/180)+r));
	pDC->Ellipse(CRect(ld,rt));
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();
	//黑色鱼眼，见图A-5
	long r1=R/8;//鱼眼半径
	NewBrush.CreateSolidBrush(RGB(0,0,0));
	pOldBrush=pDC->SelectObject(&NewBrush);
    ld=CPoint(Round(r*cos((Theta+90)*PI/180)-r1),Round(r*sin((Theta+90)*PI/180)-r1));
    rt=CPoint(Round(r*cos((Theta+90)*PI/180)+r1),Round(r*sin((Theta+90)*PI/180)+r1));
	pDC->Ellipse(CRect(ld,rt));
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();	
	//白色鱼眼，见图A-6
	NewPen.CreatePen(PS_SOLID,1,RGB(255,255,255)); 
	pOldPen=pDC->SelectObject(&NewPen);
	ld=CPoint(Round(r*cos((Theta-90)*PI/180)-r1),Round(r*sin((Theta-90)*PI/180)-r1));
    rt=CPoint(Round(r*cos((Theta-90)*PI/180)+r1),Round(r*sin((Theta-90)*PI/180)+r1));
	pDC->Ellipse(CRect(ld,rt));
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
	//绘制空心圆
	pOldBrush=(CBrush*)pDC->SelectStockObject(NULL_BRUSH);
	ld=CPoint(-R,-R);
	rt=CPoint(R,R);
	pDC->Ellipse(CRect(ld,rt));
	pDC->SelectObject(pOldBrush);
}

void CTestView::OnMdraw() //菜单函数
{
	// TODO: Add your command handler code here
	bPlay=bPlay?FALSE:TRUE;
	if (bPlay)//设置定时器
		SetTimer(1,1,NULL);	
	else
		KillTimer(1);
}

void CTestView::OnUpdateMdraw(CCmdUI* pCmdUI)//菜单按钮控制函数 
{
	// TODO: Add your command update UI handler code here
	if(bPlay)
	{
		pCmdUI->SetCheck(TRUE);
		pCmdUI->SetText("停止");
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
		pCmdUI->SetText("播放");
	}		
}

void CTestView::OnTimer(UINT nIDEvent) //定时器函数
{
	// TODO: Add your message handler code here and/or call default
	Theta-=1;
	if(Theta==0)
		Theta=360;
	Invalidate(FALSE);
	CView::OnTimer(nIDEvent);
}
