// testMessageProcessingDoc.cpp : implementation of the CTestMessageProcessingDoc class
//

#include "stdafx.h"
#include "testMessageProcessing.h"

#include "testMessageProcessingDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingDoc

IMPLEMENT_DYNCREATE(CTestMessageProcessingDoc, CDocument)

BEGIN_MESSAGE_MAP(CTestMessageProcessingDoc, CDocument)
	//{{AFX_MSG_MAP(CTestMessageProcessingDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingDoc construction/destruction

CTestMessageProcessingDoc::CTestMessageProcessingDoc()
{
	// TODO: add one-time construction code here

}

CTestMessageProcessingDoc::~CTestMessageProcessingDoc()
{
}

BOOL CTestMessageProcessingDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingDoc serialization

void CTestMessageProcessingDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingDoc diagnostics

#ifdef _DEBUG
void CTestMessageProcessingDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTestMessageProcessingDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestMessageProcessingDoc commands
