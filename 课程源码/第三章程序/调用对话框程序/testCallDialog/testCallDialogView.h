// testCallDialogView.h : interface of the CTestCallDialogView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTCALLDIALOGVIEW_H__F48425D5_09E8_4AB5_9875_9D1763F75E81__INCLUDED_)
#define AFX_TESTCALLDIALOGVIEW_H__F48425D5_09E8_4AB5_9875_9D1763F75E81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "InputDlg.h"//包含对话框头文件
#include "InputDlg2.h"//包含对话框头文件


class CTestCallDialogView : public CView
{
protected: // create from serialization only
	CTestCallDialogView();
	DECLARE_DYNCREATE(CTestCallDialogView)

// Attributes
public:
	CTestCallDialogDoc* GetDocument();
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestCallDialogView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTestCallDialogView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTestCallDialogView)
	afx_msg void OnRectangle();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in testCallDialogView.cpp
inline CTestCallDialogDoc* CTestCallDialogView::GetDocument()
   { return (CTestCallDialogDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCALLDIALOGVIEW_H__F48425D5_09E8_4AB5_9875_9D1763F75E81__INCLUDED_)
