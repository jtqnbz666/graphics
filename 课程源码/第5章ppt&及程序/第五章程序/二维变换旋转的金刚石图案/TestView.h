// TestView.h : interface of the CTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTVIEW_H__6DC176FC_B545_4EDA_9B99_E1F77854170A__INCLUDED_)
#define AFX_TESTVIEW_H__6DC176FC_B545_4EDA_9B99_E1F77854170A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Transform2.h"

class CTestView : public CView
{
protected: // create from serialization only
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// Attributes
public:
	CTestDoc* GetDocument();

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	void ReadPoint();//读入顶点
	void DoubleBuffer(CDC* pDC);//双缓冲
	void Diamond(CDC* pDC);//绘制金刚石图案函数
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif	
protected:
	
// Generated message map functions
protected:
	CP2 P[20];   //定义动态数组
	int n;    //定义等分点个数
	BOOL bPlay;//动画开关
	CTransform2 tran;//变换对象
	//{{AFX_MSG(CTestView)
	afx_msg void OnDrawpic();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnUpdateDrawpic(CCmdUI* pCmdUI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TestView.cpp
inline CTestDoc* CTestView::GetDocument()
   { return (CTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVIEW_H__6DC176FC_B545_4EDA_9B99_E1F77854170A__INCLUDED_)
